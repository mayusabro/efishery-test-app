# eFishery Test App

Menampilkan harga, ukuran, wilayah dari setiap ikan

## Installation

Install aplikasi dari link di bawah ini
https://drive.google.com/file/d/1rbMuY1U0dWHP3ZOAYuSlb8kgcjHztaj-/view?usp=sharing

## Usage

### Sorting dan Filter
Dengan meng-klik tanda sortir di kiri atas terdapat menu sortir, filter, dan pencarian berdasarkan nama ikan

### Menambah Ikan
Dengan meng-klik tanda 'plus' di kanan atas, menampilkan form Ikan baru.
Ikan yang di tambahkan di simpan secara local dan dikirim ke remote secara background menggunakan work manager
