/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to Color(in) writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.efisheryapp.ui.themes

import androidx.compose.ui.graphics.Color

enum class MaterialColor(val tones: Map<Int, Color> = emptyMap(), val accentTones: Map<Int, Color> = emptyMap(), val singleColor: Color? = null) {
    RED(
        tones = mapOf(
            50 to Color(0xFFFFEBEE),
            100 to Color(0xFFFFCDD2),
            200 to Color(0xFFEF9A9A),
            300 to Color(0xFFE57373),
            400 to Color(0xFFEF5350),
            500 to Color(0xFFF44336),
            600 to Color(0xFFE53935),
            700 to Color(0xFFD32F2F),
            800 to Color(0xFFC62828),
            900 to Color(0xFFB71C1C)
        ),
        accentTones = mapOf(
            100 to Color(0xFFFF8A80),
            200 to Color(0xFFFF5252),
            400 to Color(0xFFFF1744),
            700 to Color(0xFFD50000)
        )
    ),
    PINK(
        tones = mapOf(
            50 to Color(0xFFFCE4EC),
            100 to Color(0xFFF8BBD0),
            200 to Color(0xFFF48FB1),
            300 to Color(0xFFF06292),
            400 to Color(0xFFEC407A),
            500 to Color(0xFFE91E63),
            600 to Color(0xFFD81B60),
            700 to Color(0xFFC2185B),
            800 to Color(0xFFAD1457),
            900 to Color(0xFF880E4F)
        ),
        accentTones = mapOf(
            100 to Color(0xFFFF80AB),
            200 to Color(0xFFFF4081),
            400 to Color(0xFFF50057),
            700 to Color(0xFFC51162)
        )
    ),
    PURPLE(
        tones = mapOf(
            50 to Color(0xFFF3E5F5),
            100 to Color(0xFFE1BEE7),
            200 to Color(0xFFCE93D8),
            300 to Color(0xFFBA68C8),
            400 to Color(0xFFAB47BC),
            500 to Color(0xFF9C27B0),
            600 to Color(0xFF8E24AA),
            700 to Color(0xFF7B1FA2),
            800 to Color(0xFF6A1B9A),
            900 to Color(0xFF4A148C)
        ),
        accentTones = mapOf(
            100 to Color(0xFFEA80FC),
            200 to Color(0xFFE040FB),
            400 to Color(0xFFD500F9),
            700 to Color(0xFFAA00FF)
        )
    ),
    DEEP_PURPLE(
        tones = mapOf(
            50 to Color(0xFFEDE7F6),
            100 to Color(0xFFD1C4E9),
            200 to Color(0xFFB39DDB),
            300 to Color(0xFF9575CD),
            400 to Color(0xFF7E57C2),
            500 to Color(0xFF673AB7),
            600 to Color(0xFF5E35B1),
            700 to Color(0xFF512DA8),
            800 to Color(0xFF4527A0),
            900 to Color(0xFF311B92)
        ),
        accentTones = mapOf(
            100 to Color(0xFFB388FF),
            200 to Color(0xFF7C4DFF),
            400 to Color(0xFF651FFF),
            700 to Color(0xFF6200EA)
        )
    ),
    INDIGO(
        tones = mapOf(
            50 to Color(0xFFE8EAF6),
            100 to Color(0xFFC5CAE9),
            200 to Color(0xFF9FA8DA),
            300 to Color(0xFF7986CB),
            400 to Color(0xFF5C6BC0),
            500 to Color(0xFF3F51B5),
            600 to Color(0xFF3949AB),
            700 to Color(0xFF303F9F),
            800 to Color(0xFF283593),
            900 to Color(0xFF1A237E)
        ),
        accentTones = mapOf(
            100 to Color(0xFF8C9EFF),
            200 to Color(0xFF536DFE),
            400 to Color(0xFF3D5AFE),
            700 to Color(0xFF304FFE)
        )
    ),
    BLUE(
        tones = mapOf(
            50 to Color(0xFFE3F2FD),
            100 to Color(0xFFBBDEFB),
            200 to Color(0xFF90CAF9),
            300 to Color(0xFF64B5F6),
            400 to Color(0xFF42A5F5),
            500 to Color(0xFF2196F3),
            600 to Color(0xFF1E88E5),
            700 to Color(0xFF1976D2),
            800 to Color(0xFF1565C0),
            900 to Color(0xFF0D47A1)
        ),
        accentTones = mapOf(
            100 to Color(0xFF82B1FF),
            200 to Color(0xFF448AFF),
            400 to Color(0xFF2979FF),
            700 to Color(0xFF2962FF)
        )
    ),
    LIGHT_BLUE(
        tones = mapOf(
            50 to Color(0xFFE1F5FE),
            100 to Color(0xFFB3E5FC),
            200 to Color(0xFF81D4FA),
            300 to Color(0xFF4FC3F7),
            400 to Color(0xFF29B6F6),
            500 to Color(0xFF03A9F4),
            600 to Color(0xFF039BE5),
            700 to Color(0xFF0288D1),
            800 to Color(0xFF0277BD),
            900 to Color(0xFF01579B)
        ),
        accentTones = mapOf(
            100 to Color(0xFF80D8FF),
            200 to Color(0xFF40C4FF),
            400 to Color(0xFF00B0FF),
            700 to Color(0xFF0091EA)
        )
    ),
    CYAN(
        tones = mapOf(
            50 to Color(0xFFE0F7FA),
            100 to Color(0xFFB2EBF2),
            200 to Color(0xFF80DEEA),
            300 to Color(0xFF4DD0E1),
            400 to Color(0xFF26C6DA),
            500 to Color(0xFF00BCD4),
            600 to Color(0xFF00ACC1),
            700 to Color(0xFF0097A7),
            800 to Color(0xFF00838F),
            900 to Color(0xFF006064)
        ),
        accentTones = mapOf(
            100 to Color(0xFF84FFFF),
            200 to Color(0xFF18FFFF),
            400 to Color(0xFF00E5FF),
            700 to Color(0xFF00B8D4)
        )
    ),
    TEAL(
        tones = mapOf(
            50 to Color(0xFFE0F2F1),
            100 to Color(0xFFB2DFDB),
            200 to Color(0xFF80CBC4),
            300 to Color(0xFF4DB6AC),
            400 to Color(0xFF26A69A),
            500 to Color(0xFF009688),
            600 to Color(0xFF00897B),
            700 to Color(0xFF00796B),
            800 to Color(0xFF00695C),
            900 to Color(0xFF004D40)
        ),
        accentTones = mapOf(
            100 to Color(0xFFA7FFEB),
            200 to Color(0xFF64FFDA),
            400 to Color(0xFF1DE9B6),
            700 to Color(0xFF00BFA5)
        )
    ),
    GREEN(
        tones = mapOf(
            50 to Color(0xFFE8F5E9),
            100 to Color(0xFFC8E6C9),
            200 to Color(0xFFA5D6A7),
            300 to Color(0xFF81C784),
            400 to Color(0xFF66BB6A),
            500 to Color(0xFF4CAF50),
            600 to Color(0xFF43A047),
            700 to Color(0xFF388E3C),
            800 to Color(0xFF2E7D32),
            900 to Color(0xFF1B5E20)
        ),
        accentTones = mapOf(
            100 to Color(0xFFB9F6CA),
            200 to Color(0xFF69F0AE),
            400 to Color(0xFF00E676),
            700 to Color(0xFF00C853)
        )
    ),
    LIGHT_GREEN(
        tones = mapOf(
            50 to Color(0xFFF1F8E9),
            100 to Color(0xFFDCEDC8),
            200 to Color(0xFFC5E1A5),
            300 to Color(0xFFAED581),
            400 to Color(0xFF9CCC65),
            500 to Color(0xFF8BC34A),
            600 to Color(0xFF7CB342),
            700 to Color(0xFF689F38),
            800 to Color(0xFF558B2F),
            900 to Color(0xFF33691E)
        ),
        accentTones = mapOf(
            100 to Color(0xFFCCFF90),
            200 to Color(0xFFB2FF59),
            400 to Color(0xFF76FF03),
            700 to Color(0xFF64DD17)
        )
    ),
    LIME(
        tones = mapOf(
            50 to Color(0xFFF9FBE7),
            100 to Color(0xFFF0F4C3),
            200 to Color(0xFFE6EE9C),
            300 to Color(0xFFDCE775),
            400 to Color(0xFFD4E157),
            500 to Color(0xFFCDDC39),
            600 to Color(0xFFC0CA33),
            700 to Color(0xFFAFB42B),
            800 to Color(0xFF9E9D24),
            900 to Color(0xFF827717)
        ),
        accentTones = mapOf(
            100 to Color(0xFFF4FF81),
            200 to Color(0xFFEEFF41),
            400 to Color(0xFFC6FF00),
            700 to Color(0xFFAEEA00)
        )
    ),
    YELLOW(
        tones = mapOf(
            50 to Color(0xFFFFFDE7),
            100 to Color(0xFFFFF9C4),
            200 to Color(0xFFFFF59D),
            300 to Color(0xFFFFF176),
            400 to Color(0xFFFFEE58),
            500 to Color(0xFFFFEB3B),
            600 to Color(0xFFFDD835),
            700 to Color(0xFFFBC02D),
            800 to Color(0xFFF9A825),
            900 to Color(0xFFF57F17)
        ),
        accentTones = mapOf(
            100 to Color(0xFFFFFF8D),
            200 to Color(0xFFFFFF00),
            400 to Color(0xFFFFEA00),
            700 to Color(0xFFFFD600)
        )
    ),
    AMBER(
        tones = mapOf(
            50 to Color(0xFFFFF8E1),
            100 to Color(0xFFFFECB3),
            200 to Color(0xFFFFE082),
            300 to Color(0xFFFFD54F),
            400 to Color(0xFFFFCA28),
            500 to Color(0xFFFFC107),
            600 to Color(0xFFFFB300),
            700 to Color(0xFFFFA000),
            800 to Color(0xFFFF8F00),
            900 to Color(0xFFFF6F00)
        ),
        accentTones = mapOf(
            100 to Color(0xFFFFE57F),
            200 to Color(0xFFFFD740),
            400 to Color(0xFFFFC400),
            700 to Color(0xFFFFAB00)
        )
    ),
    ORANGE(
        tones = mapOf(
            50 to Color(0xFFFFF3E0),
            100 to Color(0xFFFFE0B2),
            200 to Color(0xFFFFCC80),
            300 to Color(0xFFFFB74D),
            400 to Color(0xFFFFA726),
            500 to Color(0xFFFF9800),
            600 to Color(0xFFFB8C00),
            700 to Color(0xFFF57C00),
            800 to Color(0xFFEF6C00),
            900 to Color(0xFFE65100)
        ),
        accentTones = mapOf(
            100 to Color(0xFFFFD180),
            200 to Color(0xFFFFAB40),
            400 to Color(0xFFFF9100),
            700 to Color(0xFFFF6D00)
        )
    ),
    DEEP_ORANGE(
        tones = mapOf(
            50 to Color(0xFFFBE9E7),
            100 to Color(0xFFFFCCBC),
            200 to Color(0xFFFFAB91),
            300 to Color(0xFFFF8A65),
            400 to Color(0xFFFF7043),
            500 to Color(0xFFFF5722),
            600 to Color(0xFFF4511E),
            700 to Color(0xFFE64A19),
            800 to Color(0xFFD84315),
            900 to Color(0xFFBF360C)
        ),
        accentTones = mapOf(
            100 to Color(0xFFFF9E80),
            200 to Color(0xFFFF6E40),
            400 to Color(0xFFFF3D00),
            700 to Color(0xFFDD2C0)
        )
    ),
    BROWN(
        tones = mapOf(
            50 to Color(0xFFEFEBE9),
            100 to Color(0xFFD7CCC8),
            200 to Color(0xFFBCAAA4),
            300 to Color(0xFFA1887F),
            400 to Color(0xFF8D6E63),
            500 to Color(0xFF795548),
            600 to Color(0xFF6D4C41),
            700 to Color(0xFF5D4037),
            800 to Color(0xFF4E342E),
            900 to Color(0xFF3E2723)
        )
    ),
    GREY(
        tones = mapOf(
            50 to Color(0xFFFAFAFA),
            100 to Color(0xFFF5F5F5),
            200 to Color(0xFFEEEEEE),
            300 to Color(0xFFE0E0E0),
            400 to Color(0xFFBDBDBD),
            500 to Color(0xFF9E9E9E),
            600 to Color(0xFF757575),
            700 to Color(0xFF616161),
            800 to Color(0xFF424242),
            900 to Color(0xFF212121)
        )
    ),
    BLUE_GREY(
        tones = mapOf(
            50 to Color(0xFFECEFF1),
            100 to Color(0xFFCFD8DC),
            200 to Color(0xFFB0BEC5),
            300 to Color(0xFF90A4AE),
            400 to Color(0xFF78909C),
            500 to Color(0xFF607D8B),
            600 to Color(0xFF546E7A),
            700 to Color(0xFF455A64),
            800 to Color(0xFF37474F),
            900 to Color(0xFF263238)
        )
    ),
    BLACK(singleColor = COLOR_BLACK),
    WHITE(singleColor = COLOR_WHITE);



    operator fun get(tone: Int): Color = singleColor ?: getTone(tone) ?: COLOR_BLACK
    operator fun invoke(): Color = singleColor ?: getTone(500) ?: COLOR_BLACK

    fun accent(tone: Int): Color = singleColor ?: getAccent(tone) ?: getTone(tone) ?: COLOR_BLACK

    private fun getTone(tone: Int): Color? =
        if (tones.isEmpty()) null
        else tones[tone]

    private fun getAccent(tone: Int): Color? =
        if (accentTones.isEmpty()) null
        else accentTones[tone]


}
private val COLOR_BLACK = Color(0xFF000000)
private val COLOR_WHITE = Color(0xFFFFFFFF)

fun Color.red() = Color(0xFFFF0000)
fun Color.green() = Color(0xFF00FF00)
fun Color.blue() = Color(0xFF0000FF)

private fun colorInt(red: Int, green: Int, blue: Int) = (red shl 16) + (green shl 8) + blue