package com.test.efisheryapp.ui.themes

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val LightThemeColors = lightColors(
  primary = MaterialColor.GREEN[500],
  primaryVariant = MaterialColor.GREEN[700],
  onPrimary = MaterialColor.WHITE(),
  background = MaterialColor.WHITE(),
  secondary = MaterialColor.GREEN.accent(400),
  secondaryVariant = MaterialColor.GREEN.accent(700),
  onSecondary = MaterialColor.GREEN[100],
  error = MaterialColor.RED[700],
  onBackground = MaterialColor.WHITE(),
  surface = MaterialColor.GREEN[400],
  onSurface = Color.DarkGray

  )


@Composable
fun MainTheme(
  darkTheme: Boolean = isSystemInDarkTheme(),
  content: @Composable () -> Unit
) {
  MaterialTheme(

    colors = LightThemeColors,
    typography = DefaultTypography,
    shapes = DefaultShapes,
    content = content
  )
}