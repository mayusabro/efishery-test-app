package com.test.efisheryapp

import android.app.Application
import androidx.work.Configuration
import com.google.android.gms.analytics.GoogleAnalytics
import com.google.android.gms.analytics.Tracker
import com.test.efisheryapp.features.domain.worker.FishWorkerFactory
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject


@HiltAndroidApp
class App : Application(), Configuration.Provider{

    @Inject lateinit var workerFactory: FishWorkerFactory
    private lateinit var sAnalytics: GoogleAnalytics
    private lateinit var sTracker: Tracker

    override fun getWorkManagerConfiguration(): Configuration = Configuration.Builder()
        .setMinimumLoggingLevel(android.util.Log.DEBUG)
        .setWorkerFactory(workerFactory)
        .build()

    override fun onCreate() {
        super.onCreate()
        sAnalytics = GoogleAnalytics.getInstance(this)
    }

    @Synchronized
    fun getDefaultTracker(): Tracker? {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (!::sTracker.isInitialized) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker)
        }
        return sTracker
    }

}