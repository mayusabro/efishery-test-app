package com.test.efisheryapp.features.domain.use_case

data class InsertFishUseCases(
    val insertFish: InsertFishToLocal,
    val getSizes : GetSizes,
    val getCities: GetCities
)
