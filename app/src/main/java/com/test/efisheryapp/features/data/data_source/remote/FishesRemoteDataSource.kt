package com.test.efisheryapp.features.data.data_source.remote

import com.google.gson.JsonObject
import com.test.efisheryapp.core.data_source.BaseRemoteDataSource
import com.test.efisheryapp.core.network.safeApiCall
import com.test.efisheryapp.features.domain.PostResponse
import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Fish
import com.test.efisheryapp.features.domain.entity.model.Size
import com.test.efisheryapp.features.domain.entity.response.FishResponse
import javax.inject.Inject

class FishesRemoteDataSource @Inject constructor(private val service: FishService) : BaseRemoteDataSource() {

    suspend fun getFishes(fishSize: Size?, fishCity: City?) = safeApiCall {
        val query = JsonObject()
        fishSize?.apply {
            query.addProperty("size", size.toString())
        }
        fishCity?.apply {
            query.addProperty("area_kota", city)
        }
        service.getFishList(query.toString())
    }

    suspend fun getSizes() = safeApiCall {
        service.getSizeList()
    }

    suspend fun getCities() = safeApiCall {
        service.getAreaList()
    }

    suspend fun insertFish(fish: List<Fish>): PostResponse {
        val listOf = mutableListOf<FishResponse>()
        fish.forEach {
            listOf.add(it.toRequestBody())
        }
        return service.insertFish(listOf)
    }

}