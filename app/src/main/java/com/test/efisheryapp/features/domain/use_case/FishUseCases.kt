package com.test.efisheryapp.features.domain.use_case

data class FishUseCases(
    val getFishes: GetFishes,
    val getSizes : GetSizes,
    val getCities: GetCities
)
