package com.test.efisheryapp.features.data.repository

import com.test.efisheryapp.core.network.Resource
import com.test.efisheryapp.core.network.performGetOperation
import com.test.efisheryapp.features.data.data_source.local.CityDao
import com.test.efisheryapp.features.data.data_source.remote.FishesRemoteDataSource
import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Size
import com.test.efisheryapp.features.domain.repository.CityRepository
import com.test.efisheryapp.features.domain.repository.FishRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

 class CityRepositoryImpl @Inject constructor(
     private val remoteDataSource: FishesRemoteDataSource,
     private val cityDao: CityDao
 ) : CityRepository {


     override fun getCities(): Flow<Resource<List<City?>>> = performGetOperation(
         {cityDao.getCities()},
         {remoteDataSource.getCities()},
         {
             forEach {
                 it.id = "${it.province}-${it.city}"
             }
             cityDao.insertAllCities(this)
         }
     )


 }


