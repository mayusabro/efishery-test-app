package com.test.efisheryapp.features.domain.util

sealed class OrderType {
    object Ascending: OrderType()
    object Descending: OrderType()
}
