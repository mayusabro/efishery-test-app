package com.test.efisheryapp.features.domain.repository

import com.test.efisheryapp.core.network.Resource
import com.test.efisheryapp.features.domain.PostResponse
import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Fish
import com.test.efisheryapp.features.domain.entity.model.Size
import com.test.efisheryapp.features.domain.util.FishOrder

import kotlinx.coroutines.flow.Flow
 interface FishRepository {

     fun getFishes(fishOrder: FishOrder, fishSize: Size?, fishCity: City?): Flow<Resource<List<Fish>>>
     fun getUnsentFish(): List<Fish>

     suspend fun insertFishToLocal(fish: Fish)
     suspend fun insertFish(fishes: List<Fish>): PostResponse
 }


