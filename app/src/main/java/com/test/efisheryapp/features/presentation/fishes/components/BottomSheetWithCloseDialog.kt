package com.test.efisheryapp.features.presentation.fishes.components
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Preview
@Composable
fun BottomSheetWithCloseDialog(
    modifier: Modifier = Modifier,
    onClosePressed: () -> Unit = {},
    closeButtonColor: Color = MaterialTheme.colors.onSurface,
    content: @Composable() () -> Unit = {}
) {

    Column(modifier.fillMaxWidth()) {
        Box(Modifier.fillMaxWidth(), contentAlignment = Alignment.CenterEnd) {
            IconButton(
                onClick =  onClosePressed,
            ) {
                Icon(
                    tint = MaterialTheme.colors.onSurface,
                    imageVector = Icons.Default.Close,
                    contentDescription = "Close Dialog"
                )
            }
        }
        Divider(color = MaterialTheme.colors.background, thickness = 0.7.dp, modifier = Modifier.alpha(0.5f))
        Box(Modifier.fillMaxWidth()) {
            content()
        }

    }
}