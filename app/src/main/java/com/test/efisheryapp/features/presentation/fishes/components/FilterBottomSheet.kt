package com.test.efisheryapp.features.presentation.fishes.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

import androidx.compose.ui.unit.dp
import com.test.efisheryapp.features.presentation.fishes.FishEvent
import com.test.efisheryapp.features.presentation.fishes.FishViewModel
import com.test.efisheryapp.features.presentation.util.FilterBottomSheetScreen

@Composable
fun SheetLayout(
    modifier: Modifier = Modifier,
    viewModel: FishViewModel,
    currentScreen: FilterBottomSheetScreen,
    onCloseBottomSheet :()->Unit
) {
    BottomSheetWithCloseDialog(modifier, onCloseBottomSheet){
        when(currentScreen){
            FilterBottomSheetScreen.CityScreen -> CityScreen(viewModel,onCloseBottomSheet)
            FilterBottomSheetScreen.SizeScreen -> SizeScreen(viewModel,onCloseBottomSheet)
        }
    }
}

@Composable
fun CityScreen(viewModel: FishViewModel, onCloseBottomSheet: () -> Unit) {
    Box(modifier = Modifier
        .fillMaxWidth()
        .wrapContentHeight()
    ){
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            contentPadding = PaddingValues(horizontal = 0.dp, vertical = 32.dp)
        ) {
            items(viewModel.state.value.cities) { city ->
                TextButton(modifier = Modifier.fillMaxWidth().padding(16.dp, 4.dp), onClick = {
                    viewModel.onEvent(FishEvent.FilterCity(city))
                    onCloseBottomSheet()
                }) {
                    Text(city?.getFormattedCity() ?: "Semua Daerah", style = MaterialTheme.typography.body1, color = MaterialTheme.colors.primaryVariant)
                }
            }
        }
    }
}
@Composable
fun SizeScreen(viewModel: FishViewModel, onCloseBottomSheet: () -> Unit) {
    Box(modifier = Modifier
        .fillMaxSize()
        .wrapContentHeight()
    ){
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            contentPadding = PaddingValues(horizontal = 0.dp, vertical = 32.dp)
        ) {
            items(viewModel.state.value.sizes) { size ->
                TextButton(modifier = Modifier.fillMaxWidth().padding(16.dp, 4.dp), onClick = {
                    viewModel.onEvent(FishEvent.FilterSize(size))
                    onCloseBottomSheet()
                }) {
                    Text(size?.getFormattedSize() ?: "Semua Ukuran", style = MaterialTheme.typography.body1, color = MaterialTheme.colors.primaryVariant)
                }
            }
        }
    }
}