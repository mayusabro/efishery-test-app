package com.test.efisheryapp.features.domain.worker

import android.content.Context
import androidx.compose.ui.platform.LocalContext
import androidx.work.*
import com.test.efisheryapp.features.domain.repository.FishRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FishWorkerFactory @Inject constructor (private val repository: FishRepository) : WorkerFactory() {

    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters
    ): ListenableWorker {
        // This only handles a single Worker, please don’t do this!!
        // See below for a better way using DelegatingWorkerFactory
        return CheckUnsentWorker(appContext, workerParameters, repository)

    }
}

