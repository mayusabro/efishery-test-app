package com.test.efisheryapp.features.presentation.add_fish

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.work.*
import com.google.gson.Gson
import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Fish
import com.test.efisheryapp.features.domain.entity.model.Size
import com.test.efisheryapp.features.domain.worker.CheckUnsentWorker
import com.test.efisheryapp.features.presentation.add_fish.components.AddFishSheetLayout
import com.test.efisheryapp.features.presentation.fishes.FishEvent
import com.test.efisheryapp.features.presentation.util.FilterBottomSheetScreen
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit

@ExperimentalMaterialApi
@Composable
fun AddFishScreen(
    navController: NavController,
    viewModel: AddFishViewModel = hiltViewModel()
){
    val context = LocalContext.current
    val state = viewModel.state.value
    val scaffoldState = rememberBottomSheetScaffoldState()
    val scope = rememberCoroutineScope()

    var currentBottomSheet: FilterBottomSheetScreen? by remember{
        mutableStateOf(null)
    }
    if(scaffoldState.bottomSheetState.isCollapsed)
        currentBottomSheet = null

    // to set the current sheet to null when the bottom sheet closes
    if(scaffoldState.bottomSheetState.isCollapsed)
        currentBottomSheet = null

    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when(event) {
                is AddFishViewModel.UiEvent.ShowSnackbar -> {
                    scaffoldState.snackbarHostState.showSnackbar(
                        message = event.message
                    )
                }
                is AddFishViewModel.UiEvent.SaveNote -> {
                    navController.navigateUp()
                }
            }
        }
    }

    val closeSheet: () -> Unit = {
        scope.launch {
            scaffoldState.bottomSheetState.collapse()

        }
    }
    var komoditas by remember { mutableStateOf("") }
    var price by remember { mutableStateOf("") }

    val openSheet: (FilterBottomSheetScreen) -> Unit = {
        scope.launch {
            currentBottomSheet = it
            scaffoldState.bottomSheetState.expand() }

    }

    BottomSheetScaffold(

        scaffoldState = scaffoldState,
        backgroundColor = MaterialTheme.colors.primary,
        sheetShape = RoundedCornerShape(32.dp, 32.dp,0.dp,0.dp),
        sheetBackgroundColor = MaterialTheme.colors.background,
        sheetContent = {
            currentBottomSheet?.let { currentSheet ->
                AddFishSheetLayout(viewModel = viewModel, currentScreen = currentSheet, onCloseBottomSheet = closeSheet)
            }
        },sheetPeekHeight = 0.dp
    ){
        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp, 16.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = "Tambah Ikan",
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.h5
                )

            }

            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(
                        color = MaterialTheme.colors.background,
                        shape = RoundedCornerShape(16.dp, 16.dp, 0.dp, 0.dp)
                    )
            ) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(32.dp)) {

                    Text("Komoditas :", style = MaterialTheme.typography.caption, color = MaterialTheme.colors.onSurface)

                    OutlinedTextField(
                        modifier = Modifier.fillMaxWidth(),
                        value = komoditas,
                        singleLine = true,
                        colors = TextFieldDefaults.outlinedTextFieldColors(
                            textColor = MaterialTheme.colors.onSurface
                        ),
                        onValueChange = {komoditas = it},
                        textStyle = MaterialTheme.typography.body1,
                    )

                    Spacer(modifier = Modifier.height(4.dp))
                    Text("Harga :", style = MaterialTheme.typography.caption, color = MaterialTheme.colors.onSurface)
                    OutlinedTextField(
                        modifier = Modifier.fillMaxWidth(),
                        value = price,
                        singleLine = true,
                        colors = TextFieldDefaults.outlinedTextFieldColors(
                            textColor = MaterialTheme.colors.onSurface
                        ),
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        onValueChange = {price = it},
                        textStyle = MaterialTheme.typography.body1,
                    )

                    Spacer(modifier = Modifier.height(4.dp))
                    Text("Ukuran :", style = MaterialTheme.typography.caption, color = MaterialTheme.colors.onSurface)
                    Button(
                        modifier = Modifier
                            .fillMaxWidth(),
                        onClick = {
                            viewModel.getSizes()
                            openSheet(FilterBottomSheetScreen.SizeScreen)
                        },
                        shape= RoundedCornerShape(8.dp),
                    ){
                        Text(modifier = Modifier.weight(1f),text = state.fishFilterSize?.getFormattedSize()?:"Pilih ukuran", style = MaterialTheme.typography.body2)
                        Icon(imageVector = Icons.Default.KeyboardArrowDown, contentDescription = "")
                    }
                    Spacer(modifier = Modifier.height(4.dp))
                    Text("Daerah :", style = MaterialTheme.typography.caption, color = MaterialTheme.colors.onSurface)
                    Button(
                        modifier = Modifier
                            .fillMaxWidth(),
                        onClick = {
                            viewModel.getCities()
                            openSheet(FilterBottomSheetScreen.CityScreen)
                        },
                        shape= RoundedCornerShape(8.dp),
                    ){
                        Text(modifier = Modifier.weight(1f),text = state.fishFilterCity?.getFormattedCity()?:"Pilih Daerah", style = MaterialTheme.typography.body2)
                        Icon(imageVector = Icons.Default.KeyboardArrowDown, contentDescription = "")
                    }
                    Spacer(modifier = Modifier.weight(1f))
                    Button(modifier = Modifier.fillMaxWidth(),
                        shape= RoundedCornerShape(8.dp),
                        onClick = {
                            if(komoditas.trim().isEmpty()){
                                viewModel.onEvent(AddFishEvent.ErrorInsertFish("Komoditas tidak boleh kosong"))
                                return@Button
                            }
                            if(price.trim().isEmpty()){
                                viewModel.onEvent(AddFishEvent.ErrorInsertFish("Harga tidak boleh kosong"))
                                return@Button
                            }
                            if(state.fishFilterCity == null){
                                viewModel.onEvent(AddFishEvent.ErrorInsertFish("Daerah tidak boleh kosong"))
                                return@Button
                            }
                            if(state.fishFilterSize == null){
                                viewModel.onEvent(AddFishEvent.ErrorInsertFish("Ukuran tidak boleh kosong"))
                                return@Button
                            }
                            viewModel.onEvent(AddFishEvent.InsertFish(Fish(
                                komoditas = komoditas,
                                price = price.toIntOrNull()?:0,
                                area_kota = state.fishFilterCity?.city?:"",
                                area_provinsi = state.fishFilterCity?.province?:"",
                                size = state.fishFilterSize?.size?:0
                            ).fillAutoGenerate(),context))

                        }
                    ) {
                        Text(text = "Tambah", style = MaterialTheme.typography.body1)
                    }
                    TextButton(
                        modifier = Modifier
                            .fillMaxWidth(),
                        onClick = {
                            navController.navigateUp()
                        },
                        shape= RoundedCornerShape(8.dp),
                    ){
                        Text(modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Center, text = "Batal", style = MaterialTheme.typography.body2, color = MaterialTheme.colors.onSurface)
                    }
                }
            }
        }
    }
}