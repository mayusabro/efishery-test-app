package com.test.efisheryapp.features.domain.repository

import com.test.efisheryapp.core.network.Resource
import com.test.efisheryapp.features.domain.entity.model.City
import kotlinx.coroutines.flow.Flow

interface CityRepository {
    fun getCities() : Flow<Resource<List<City?>>>
}