package com.test.efisheryapp.features.domain.worker

import android.content.Context
import android.util.Log
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import com.google.gson.Gson
import com.test.efisheryapp.core.network.safeApiCall
import com.test.efisheryapp.features.domain.entity.model.Fish
import com.test.efisheryapp.features.domain.repository.FishRepository
import com.test.efisheryapp.features.presentation.add_fish.AddFishViewModel
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltWorker
class CheckUnsentWorker @AssistedInject constructor(@Assisted appContext: Context, @Assisted val params: WorkerParameters, val fishRepository: FishRepository) : CoroutineWorker(appContext, params) {
       override suspend fun doWork(): Result {
              return try {
                     val json = inputData.getString("data")
                     val newFish = Gson().fromJson(json, Fish::class.java)
                     fishRepository.insertFish(listOf(newFish))
                     Result.success()
              } catch (e : Exception){
                     Result.retry()
              }
       }
}
