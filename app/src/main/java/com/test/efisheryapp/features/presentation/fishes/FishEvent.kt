package com.test.efisheryapp.features.presentation.fishes

import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Size
import com.test.efisheryapp.features.domain.util.FishOrder

sealed class FishEvent {
    data class Order(val fishOrder: FishOrder): FishEvent()
    data class FilterSize(val size: Size?): FishEvent()
    data class FilterCity(val city: City?): FishEvent()
    data class SearchKey(val key: String?): FishEvent()

    object ToggleFilterSection: FishEvent()
}