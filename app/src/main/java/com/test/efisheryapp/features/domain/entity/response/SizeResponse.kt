package com.test.efisheryapp.features.domain.entity.response

data class SizeResponse(
    val size : String
)