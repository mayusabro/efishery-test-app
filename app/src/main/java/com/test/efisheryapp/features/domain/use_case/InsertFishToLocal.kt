package com.test.efisheryapp.features.domain.use_case

import com.test.efisheryapp.features.domain.entity.model.Fish
import com.test.efisheryapp.features.domain.repository.FishRepository
import kotlinx.coroutines.flow.map

class InsertFishToLocal(
    private val repository : FishRepository
) {
    suspend operator fun invoke(fish: Fish) = repository.insertFishToLocal(fish)

}