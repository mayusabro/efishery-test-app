package com.test.efisheryapp.features.presentation.add_fish.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

import androidx.compose.ui.unit.dp
import com.test.efisheryapp.features.presentation.add_fish.AddFishEvent
import com.test.efisheryapp.features.presentation.add_fish.AddFishViewModel
import com.test.efisheryapp.features.presentation.util.BottomSheetWithCloseDialog
import com.test.efisheryapp.features.presentation.util.FilterBottomSheetScreen

@Composable
fun AddFishSheetLayout(
    modifier: Modifier = Modifier,
    viewModel: AddFishViewModel,
    currentScreen: FilterBottomSheetScreen,
    onCloseBottomSheet :()->Unit
) {
    BottomSheetWithCloseDialog(modifier, onCloseBottomSheet){
        when(currentScreen){
            FilterBottomSheetScreen.CityScreen -> AddFishCityScreen(viewModel,onCloseBottomSheet)
            FilterBottomSheetScreen.SizeScreen -> AddFishSizeScreen(viewModel,onCloseBottomSheet)
        }
    }
}

@Composable
fun AddFishCityScreen(viewModel: AddFishViewModel, onCloseBottomSheet: () -> Unit) {
    Box(modifier = Modifier
        .fillMaxWidth()
        .wrapContentHeight()
    ){
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            contentPadding = PaddingValues(horizontal = 0.dp, vertical = 32.dp)
        ) {
            items(viewModel.state.value.cities) { city ->
                TextButton(modifier = Modifier.fillMaxWidth().padding(16.dp, 4.dp), onClick = {
                    viewModel.onEvent(AddFishEvent.SetCity(city))
                    onCloseBottomSheet()
                }) {
                    Text(city.getFormattedCity() , style = MaterialTheme.typography.body1, color = MaterialTheme.colors.primaryVariant)
                }
            }
        }
    }
}
@Composable
fun AddFishSizeScreen(viewModel: AddFishViewModel, onCloseBottomSheet: () -> Unit) {
    Box(modifier = Modifier
        .fillMaxSize()
        .wrapContentHeight()
    ){
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            contentPadding = PaddingValues(horizontal = 0.dp, vertical = 32.dp)
        ) {
            items(viewModel.state.value.sizes) { size ->
                TextButton(modifier = Modifier.fillMaxWidth().padding(16.dp, 4.dp), onClick = {
                    viewModel.onEvent(AddFishEvent.SetSize(size))
                    onCloseBottomSheet()
                }) {
                    Text(size.getFormattedSize(), style = MaterialTheme.typography.body1, color = MaterialTheme.colors.primaryVariant)
                }
            }
        }
    }
}