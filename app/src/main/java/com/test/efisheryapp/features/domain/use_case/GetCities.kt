package com.test.efisheryapp.features.domain.use_case

import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.repository.CityRepository
import com.test.efisheryapp.features.domain.repository.FishRepository
import kotlinx.coroutines.flow.map

class GetCities(
    private val repository : CityRepository
) {
    operator fun invoke()= repository.getCities().map {
       mutableListOf<City?>().apply {
           add(null)
           addAll(it.data?: emptyList())
       }
    }
}