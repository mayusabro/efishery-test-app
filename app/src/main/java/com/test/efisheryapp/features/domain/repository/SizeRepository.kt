package com.test.efisheryapp.features.domain.repository

import com.test.efisheryapp.core.network.Resource
import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Size
import kotlinx.coroutines.flow.Flow

interface SizeRepository {
    fun getSizes() : Flow<Resource<List<Size?>>>
}