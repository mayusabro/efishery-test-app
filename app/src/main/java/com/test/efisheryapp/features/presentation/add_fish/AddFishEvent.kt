package com.test.efisheryapp.features.presentation.add_fish

import android.content.Context
import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Fish
import com.test.efisheryapp.features.domain.entity.model.Size

sealed class AddFishEvent{
    data class SetSize(val size : Size) : AddFishEvent()
    data class SetCity(val city : City) : AddFishEvent()
    data class InsertFish(val fish : Fish, val context: Context) : AddFishEvent()
    data class ErrorInsertFish(val message : String) : AddFishEvent()
}