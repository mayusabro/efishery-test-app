package com.test.efisheryapp.features.domain

abstract class BaseResponse<T> {
    abstract fun toLocalData() : T
}

fun <V : BaseResponse<T>, T> List<V>.toLocalData(condition : V.()->Boolean = { true }): List<T> {
    val list = mutableListOf<T>()
    forEach {
        if (condition(it)) {
            list.add(it.toLocalData())
        }
    }
    return list.toList()

}

data class PostResponse(
    val updatedRange : String?
)