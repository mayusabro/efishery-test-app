package com.test.efisheryapp.features.presentation.fishes

import android.util.Log
import androidx.compose.animation.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Sort
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.test.efisheryapp.features.presentation.fishes.components.EmptyContent
import com.test.efisheryapp.features.presentation.util.Screen
import com.test.efisheryapp.features.presentation.fishes.components.FilterSection
import com.test.efisheryapp.features.presentation.fishes.components.FishCard
import com.test.efisheryapp.features.presentation.fishes.components.SheetLayout
import com.test.efisheryapp.features.presentation.util.FilterBottomSheetScreen
import kotlinx.coroutines.launch

@ExperimentalMaterialApi
@Composable
fun FishScreen(
    navController: NavController,
    viewModel: FishViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val scaffoldState = rememberBottomSheetScaffoldState()
    val scope = rememberCoroutineScope()

    val filterKey = remember { mutableStateOf(TextFieldValue("")) }

    var currentBottomSheet: FilterBottomSheetScreen? by remember{
        mutableStateOf(null)
    }

    if(scaffoldState.bottomSheetState.isCollapsed)
        currentBottomSheet = null

    // to set the current sheet to null when the bottom sheet closes
    if(scaffoldState.bottomSheetState.isCollapsed)
        currentBottomSheet = null


    val closeSheet: () -> Unit = {
        scope.launch {
            scaffoldState.bottomSheetState.collapse()

        }
    }


    val openSheet: (FilterBottomSheetScreen) -> Unit = {
        scope.launch {
            currentBottomSheet = it
            scaffoldState.bottomSheetState.expand() }

    }

    LaunchedEffect(key1 = true) {
        viewModel.getFishes()
    }

    BottomSheetScaffold(

        scaffoldState = scaffoldState,
        backgroundColor = MaterialTheme.colors.primary,
        sheetShape = RoundedCornerShape(32.dp, 32.dp,0.dp,0.dp),
        sheetBackgroundColor = MaterialTheme.colors.background,
        sheetContent = {
            currentBottomSheet?.let { currentSheet ->
                SheetLayout(viewModel = viewModel, currentScreen = currentSheet, onCloseBottomSheet = closeSheet)
            }
        },
        sheetPeekHeight = 0.dp
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp, 16.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                IconButton(onClick = {
                    viewModel.onEvent(FishEvent.ToggleFilterSection)
                }) {
                    Icon(
                        imageVector = Icons.Default.Sort,
                        contentDescription = "Sort"
                    )
                }
                Text(
                    modifier = Modifier.weight(1f),
                    text = "Harga Ikan",
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.h5
                )
                IconButton(onClick = {
                    navController.navigate(Screen.AddFishScreen.route)
                }) {
                    Icon(
                        imageVector = Icons.Default.Add,
                        contentDescription = "Sort"
                    )
                }
            }
            AnimatedVisibility(
                visible = state.isFilterSectionVisible,
                enter = fadeIn() + slideInVertically(),
                exit = fadeOut() + slideOutVertically()
            ) {
                FilterSection(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = 16.dp),
                    viewModel = viewModel,
                    filterKey,
                    onOrderChange = {
                        viewModel.onEvent(FishEvent.Order(it))
                    },
                    onCityBtnClick = {
                        viewModel.getCities()
                        openSheet(FilterBottomSheetScreen.CityScreen)
                    },
                    onSizeBtnClick = {
                        viewModel.getSizes()
                        openSheet(FilterBottomSheetScreen.SizeScreen)
                    }
                )
            }
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(
                        color = MaterialTheme.colors.background,
                        shape = RoundedCornerShape(16.dp, 16.dp, 0.dp, 0.dp)
                    )


            ) {
                LazyColumn(
                    modifier = Modifier
                        .fillMaxSize(),
                    contentPadding = PaddingValues(horizontal = 16.dp, vertical = 32.dp)
                ) {
                    var key = filterKey.value.text
                    val items = state.fishes.filter { it.komoditas?.contains(key, true)?:false }
                    if(items.isEmpty()){
                        item {EmptyContent()}
                    } else {
                        items(items) { fish ->
                            Log.d("FishViewModel", "getFishes: $fish")

                            FishCard(
                                fish = fish,
                                modifier = Modifier
                                    .fillMaxWidth()
                            )
                            Spacer(modifier = Modifier.height(16.dp))
                        }
                    }
                }
            }

        }
    }
}