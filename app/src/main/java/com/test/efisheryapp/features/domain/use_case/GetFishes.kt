package com.test.efisheryapp.features.domain.use_case

import com.test.efisheryapp.core.network.Resource
import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Fish
import com.test.efisheryapp.features.domain.entity.model.Size
import com.test.efisheryapp.features.domain.repository.FishRepository
import com.test.efisheryapp.features.domain.util.FishOrder
import com.test.efisheryapp.features.domain.util.OrderType

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GetFishes(
    private val repository : FishRepository
) {


    operator fun invoke(fishOrder: FishOrder, fishSize: Size?, fishCity: City?): Flow<Resource<List<Fish>>> {
        return repository.getFishes(fishOrder, fishSize, fishCity).map {fish->
            when(fishOrder.orderType) {
                is OrderType.Ascending -> {
                    when(fishOrder) {
                        is FishOrder.Price -> fish.copy(
                            data = fish.data?.sortedBy { it.price }
                        )
                        is FishOrder.Size -> fish.copy(
                            data = fish.data?.sortedBy { it.size }
                        )
                        is FishOrder.Time -> fish.copy(
                            data = fish.data?.sortedBy { it.timestamp }
                        )
                    }
                }
                is OrderType.Descending -> {
                    when(fishOrder) {
                        is FishOrder.Price -> fish.copy(
                            data = fish.data?.sortedByDescending { it.price }
                        )
                        is FishOrder.Size -> fish.copy(
                            data = fish.data?.sortedByDescending { it.size }
                        )
                        is FishOrder.Time -> fish.copy(
                            data = fish.data?.sortedByDescending { it.timestamp }
                        )
                    }
                }
            }
        }
    }
}