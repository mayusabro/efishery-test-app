package com.test.efisheryapp.features.presentation.add_fish

import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Fish
import com.test.efisheryapp.features.domain.entity.model.Size

data class AddFishState(
    val fish : Fish = Fish("","","","",0,0,"",0),
    val cities : List<City> = emptyList(),
    val sizes : List<Size> = emptyList(),
    val fishFilterSize : Size? = null,
    val fishFilterCity : City? = null
)