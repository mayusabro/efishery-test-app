package com.test.efisheryapp.features.presentation.util

sealed class Screen(val route: String) {
    object FishScreen: Screen("fish_screen")
    object AddFishScreen: Screen("add_fish_screen")
}