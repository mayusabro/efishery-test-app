package com.test.efisheryapp.features.data.repository

import com.test.efisheryapp.core.network.Resource
import com.test.efisheryapp.core.network.performGetOperation
import com.test.efisheryapp.features.data.data_source.local.FishDao
import com.test.efisheryapp.features.data.data_source.local.SizeDao
import com.test.efisheryapp.features.data.data_source.remote.FishesRemoteDataSource
import com.test.efisheryapp.features.domain.entity.model.Size
import com.test.efisheryapp.features.domain.repository.FishRepository
import com.test.efisheryapp.features.domain.repository.SizeRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

 class SizeRepositoryImpl @Inject constructor(
     private val remoteDataSource: FishesRemoteDataSource,
     private val sizeDao: SizeDao
 ) : SizeRepository {


     override fun getSizes(): Flow<Resource<List<Size?>>> = performGetOperation(
         {sizeDao.getSizes()},
         {remoteDataSource.getSizes()},
         {
             sizeDao.insertAllSizes(this)
         }
     )



 }


