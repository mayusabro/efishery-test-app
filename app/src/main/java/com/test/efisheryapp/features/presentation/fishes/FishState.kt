package com.test.efisheryapp.features.presentation.fishes

import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Fish
import com.test.efisheryapp.features.domain.entity.model.Size
import com.test.efisheryapp.features.domain.util.FishOrder
import com.test.efisheryapp.features.domain.util.OrderType

data class FishState (
    val fishes : List<Fish> = emptyList(),
    val cities : List<City?> = emptyList(),
    val sizes : List<Size?> = emptyList(),
    val fishOrder: FishOrder = FishOrder.Price(OrderType.Ascending),
    val fishFilterSize : Size? = null,
    val fishFilterCity : City? = null,
    val isFilterSectionVisible: Boolean = false

)