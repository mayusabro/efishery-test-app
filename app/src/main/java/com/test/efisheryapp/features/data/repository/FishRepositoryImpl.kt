package com.test.efisheryapp.features.data.repository

import com.test.efisheryapp.core.network.Resource
import com.test.efisheryapp.core.network.performGetOperation
import com.test.efisheryapp.features.data.data_source.local.FishDao
import com.test.efisheryapp.features.data.data_source.remote.FishesRemoteDataSource
import com.test.efisheryapp.features.domain.PostResponse
import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Fish
import com.test.efisheryapp.features.domain.entity.model.Size
import com.test.efisheryapp.features.domain.repository.FishRepository
import com.test.efisheryapp.features.domain.entity.response.FishResponse
import com.test.efisheryapp.features.domain.toLocalData
import com.test.efisheryapp.features.domain.util.FishOrder

import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class FishRepositoryImpl @Inject constructor(
    private val remoteDataSource: FishesRemoteDataSource,
    private val fishDao: FishDao
) : FishRepository {
    override fun getFishes(fishOrder: FishOrder, fishSize: Size?, fishCity: City?): Flow<Resource<List<Fish>>> {
        return performGetOperation(
            {
                when {
                    fishSize != null && fishCity != null -> fishDao.getFishesBySizeCity(fishSize.size, fishCity.city)
                    fishSize != null -> fishDao.getFishesBySize(fishSize.size)
                    fishCity != null -> fishDao.getFishesByCity(fishCity.city)
                    else -> fishDao.getFishes()
                }
            },
            { remoteDataSource.getFishes(fishSize, fishCity) },
            {

                fishDao.insertAllFish(toLocalData<FishResponse, Fish> { uuid != null }.apply {
                    forEach { it.synced = true }
                })
            }
        )
    }

    override fun getUnsentFish():List<Fish> = fishDao.getUnsentFishes()
    override suspend fun insertFish(fishes: List<Fish>): PostResponse = remoteDataSource.insertFish(fishes)


    override suspend fun insertFishToLocal(fish: Fish) = fishDao.insertFish(fish)




}


