package com.test.efisheryapp.features.presentation.fishes

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Size
import com.test.efisheryapp.features.domain.use_case.FishUseCases
import com.test.efisheryapp.features.domain.util.FishOrder
import com.test.efisheryapp.features.domain.util.OrderType
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class FishViewModel @Inject constructor(private val fishUseCases: FishUseCases) : ViewModel() {

  private val _state = mutableStateOf(FishState())
  val state : State<FishState> = _state

  private var getFishJob: Job? = null
  private var getCitiesJob: Job? = null
  private var getSizesJob: Job? = null

  fun onEvent(event : FishEvent){
    when (event) {
      is FishEvent.Order -> {
        if (state.value.fishOrder::class == event.fishOrder::class &&
          state.value.fishOrder.orderType == event.fishOrder.orderType
        ) {
          return
        }
        getFishes(fishOrder = event.fishOrder)
      }
      is FishEvent.ToggleFilterSection -> {
        _state.value = state.value.copy(
          isFilterSectionVisible = !state.value.isFilterSectionVisible
        )
      }
      is FishEvent.FilterCity -> {
        if (state.value.fishFilterCity?.id != event.city?.id) {
          getFishes(fishCity = event.city)

        }
      }
      is FishEvent.FilterSize -> {
        if (state.value.fishFilterSize != event.size) {
          getFishes(fishSize = event.size)
        }

      }

    }
  }


   fun getFishes(fishOrder: FishOrder = _state.value.fishOrder, fishSize : Size? = _state.value.fishFilterSize, fishCity : City? = _state.value.fishFilterCity) {
    getFishJob?.cancel()
    getFishJob = fishUseCases.getFishes(fishOrder, fishSize, fishCity).onEach { value ->
      _state.value = state.value.copy(
        fishes = value.data?: emptyList(),
        fishOrder = fishOrder,
        fishFilterSize = fishSize,
        fishFilterCity = fishCity,
      )

    }.launchIn(viewModelScope)
  }

  fun getSizes(){
    getSizesJob?.cancel()
    getSizesJob = fishUseCases.getSizes().onEach {value->
      _state.value = state.value.copy(
        sizes = value,
      )
    }.launchIn(viewModelScope)
  }

  fun getCities(){
    getCitiesJob?.cancel()
    getCitiesJob = fishUseCases.getCities().onEach {value->
      _state.value = state.value.copy(
        cities = value,
      )
    }.launchIn(viewModelScope)
  }

}