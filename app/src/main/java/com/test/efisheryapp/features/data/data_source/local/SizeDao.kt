package com.test.efisheryapp.features.data.data_source.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Fish
import com.test.efisheryapp.features.domain.entity.model.Size
import com.test.efisheryapp.features.domain.util.FishOrder
import kotlinx.coroutines.flow.Flow

@Dao
interface SizeDao {
    @Query("SELECT * FROM sizes")
    fun getSizes(): List<Size>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllSizes(list: List<Size>)
}