package com.test.efisheryapp.features.presentation

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.input.key.Key.Companion.One
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.work.*
import com.test.efisheryapp.features.domain.worker.CheckUnsentWorker
import com.test.efisheryapp.features.presentation.add_fish.AddFishScreen
import com.test.efisheryapp.features.presentation.fishes.FishScreen
import com.test.efisheryapp.features.presentation.util.Screen
import com.test.efisheryapp.ui.themes.MainTheme
import dagger.hilt.android.AndroidEntryPoint
import java.time.Duration
import java.util.concurrent.TimeUnit


@ExperimentalMaterialApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MainTheme {
                Surface(
                    color = MaterialTheme.colors.primary
                ) {
                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = Screen.FishScreen.route
                    ) {
                        composable(route = Screen.FishScreen.route) {
                            FishScreen(navController = navController)
                        }
                        composable(
                            route = Screen.AddFishScreen.route
                        ) {
                            AddFishScreen(navController = navController)
                        }
                    }
                }
            }
        }
    }
}