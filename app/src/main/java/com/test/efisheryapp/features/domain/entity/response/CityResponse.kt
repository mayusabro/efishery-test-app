package com.test.efisheryapp.features.domain.entity.response


data class CityResponse  (
    val province : String,
    val city : String
)