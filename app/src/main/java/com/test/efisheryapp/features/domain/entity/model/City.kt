package com.test.efisheryapp.features.domain.entity.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cities")
data class City (
    val province : String,
    val city : String,
    @PrimaryKey
    var id : String = "$province-$city"
){
    fun getFormattedCity() = "%s, %s".format(city, province)
}