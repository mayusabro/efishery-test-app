package com.test.efisheryapp.features.domain.entity.response

import com.test.efisheryapp.features.domain.BaseResponse
import com.test.efisheryapp.features.domain.entity.model.Fish

data class FishResponse(
    val uuid: String?,
    val area_kota: String?,
    val area_provinsi: String?,
    val komoditas: String?,
    val price: String?,
    val size: String?,
    val tgl_parsed: String?,
    val timestamp : String?
) : BaseResponse<Fish>(){
    override fun toLocalData() =
        Fish(
            uuid?:"",
            area_kota,
            area_provinsi,
            komoditas,
            price?.toInt(),
            size?.toInt(),
            tgl_parsed,
            timestamp?.toLong())

}