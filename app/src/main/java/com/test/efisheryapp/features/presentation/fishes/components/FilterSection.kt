package com.test.efisheryapp.features.presentation.fishes.components


import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.test.efisheryapp.features.domain.util.FishOrder
import com.test.efisheryapp.features.domain.util.OrderType
import com.test.efisheryapp.features.presentation.fishes.FishEvent
import com.test.efisheryapp.features.presentation.fishes.FishViewModel


@Composable
fun FilterSection(
    modifier: Modifier = Modifier,
    viewModel : FishViewModel,
    filterKey : MutableState<TextFieldValue>,
    onOrderChange: (FishOrder) -> Unit = {},
    onSizeBtnClick: () -> Unit = {},
    onCityBtnClick: () -> Unit = {}
) {
    val state = viewModel.state.value

    Column(
        modifier = modifier
    ) {

        Column(
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Center, text="Urutkan berdasarkan" , style = MaterialTheme.typography.caption)
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center

            ) {
                DefaultRadioButton(
                    text = "Waktu",
                    selected = state.fishOrder is FishOrder.Time,
                    onSelect = { onOrderChange(FishOrder.Time(state.fishOrder.orderType)) }
                )
                Spacer(modifier = Modifier.width(8.dp))
                DefaultRadioButton(
                    text = "Harga",
                    selected = state.fishOrder is FishOrder.Price,
                    onSelect = { onOrderChange(FishOrder.Price(state.fishOrder.orderType)) }
                )
                Spacer(modifier = Modifier.width(8.dp))
                DefaultRadioButton(
                    text = "Ukuran",
                    selected = state.fishOrder is FishOrder.Size,
                    onSelect = { onOrderChange(FishOrder.Size(state.fishOrder.orderType)) }
                )
            }
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                DefaultRadioButton(
                    text = "Rendah ke tinggi",
                    selected = state.fishOrder.orderType is OrderType.Ascending,
                    onSelect = {
                        onOrderChange(state.fishOrder.copy(OrderType.Ascending))
                    }
                )
                Spacer(modifier = Modifier.width(8.dp))
                DefaultRadioButton(
                    text = "Tinggi ke rendah",
                    selected = state.fishOrder.orderType is OrderType.Descending,
                    onSelect = {
                        onOrderChange(state.fishOrder.copy(OrderType.Descending))
                    }
                )

            }
            Spacer(modifier = Modifier.height(8.dp))
            Text(modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Center, text="Filter berdasarkan" , style = MaterialTheme.typography.caption)
            Spacer(modifier = Modifier.height(4.dp))
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp, 0.dp),
                horizontalArrangement = Arrangement.Center
            ) {
                Column(Modifier.weight(1f), horizontalAlignment = Alignment.CenterHorizontally) {
                    Text("Ukuran" , style = MaterialTheme.typography.overline)
                    Button(onClick = onSizeBtnClick,colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.primaryVariant)){
                        Text(modifier= Modifier.weight(1f) , text = state.fishFilterSize?.getFormattedSize() ?: "Semua Ukuran", style = MaterialTheme.typography.caption, maxLines = 1)
                        Icon(imageVector = Icons.Default.KeyboardArrowDown, contentDescription = "Dropdown size filter")
                    }
                }
                Spacer(modifier = Modifier.width(8.dp))
                Column(Modifier.weight(1f), horizontalAlignment = Alignment.CenterHorizontally) {
                    Text("Daerah" , style = MaterialTheme.typography.overline)
                    Button(onClick = onCityBtnClick, colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.primaryVariant)){
                        Text(modifier= Modifier.weight(1f) ,text = state.fishFilterCity?.getFormattedCity() ?: "Semua Daerah" , style = MaterialTheme.typography.caption, maxLines = 1)
                        Icon(imageVector = Icons.Default.KeyboardArrowDown, contentDescription = "Dropdown city filter")

                    }
                }
            }
            Spacer(modifier = Modifier.height(8.dp))
            TextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp, 0.dp),
                value = filterKey.value,
                singleLine = true,
                shape = RoundedCornerShape(8.dp),
                leadingIcon = {
                    Icon(
                        Icons.Default.Search,
                        contentDescription = "",
                        modifier = Modifier
                            .padding(15.dp)
                            .size(24.dp)
                    )
                },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    textColor = MaterialTheme.colors.onSurface,
                    backgroundColor =  MaterialTheme.colors.background
                ),
                placeholder = { Text(text = "Cari Komoditas")},
                onValueChange = {value->filterKey.value = value},
                textStyle = MaterialTheme.typography.body1,
            )
        }
    }
}