package com.test.efisheryapp.features.domain.util

sealed class FishOrder(val orderType: OrderType) {
    class Price(orderType: OrderType): FishOrder(orderType)
    class Size(orderType: OrderType): FishOrder(orderType)
    class Time(orderType: OrderType): FishOrder(orderType)

    fun copy(orderType: OrderType): FishOrder {
        return when(this) {
            is Price -> Price(orderType)
            is Size -> Size(orderType)
            is Time -> Time(orderType)
        }
    }
}