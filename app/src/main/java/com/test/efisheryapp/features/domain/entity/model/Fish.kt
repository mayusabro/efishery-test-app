package com.test.efisheryapp.features.domain.entity.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.test.efisheryapp.features.domain.entity.response.FishResponse
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = "fishes")
data class Fish(
    @PrimaryKey
    var uuid: String = "",
    var area_kota: String?,
    var area_provinsi: String?,
    var komoditas: String?,
    var price: Int?,
    var size: Int?,
    var tgl_parsed: String? = "",
    var timestamp : Long? = 0,
    var synced : Boolean = false
){

    val formattedDate
        get() =Date((timestamp?:0)*1000)
    fun getParsedDate() = SimpleDateFormat.getDateTimeInstance(SimpleDateFormat.SHORT, SimpleDateFormat.SHORT).format(formattedDate)

    fun toRequestBody()=FishResponse(
        uuid,
        area_kota,
        area_provinsi,
        komoditas,
        price.toString(),
        size.toString(),
        tgl_parsed,
        timestamp.toString()
    )

    fun fillAutoGenerate(): Fish {
        uuid = UUID.randomUUID().toString()
        tgl_parsed = SimpleDateFormat.getDateTimeInstance().format(Date())
        timestamp = Date().time
        return this
    }
}