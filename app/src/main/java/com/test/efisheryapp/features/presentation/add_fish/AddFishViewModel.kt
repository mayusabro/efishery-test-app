package com.test.efisheryapp.features.presentation.add_fish

import android.content.Context
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.work.*
import com.google.gson.Gson
import com.test.efisheryapp.core.network.safeApiCall
import com.test.efisheryapp.features.domain.entity.model.Fish
import com.test.efisheryapp.features.domain.use_case.InsertFishUseCases
import com.test.efisheryapp.features.domain.worker.CheckUnsentWorker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class AddFishViewModel @Inject constructor(private val useCases: InsertFishUseCases) : ViewModel() {
    private val _state = mutableStateOf(AddFishState())
    val state : State<AddFishState> = _state

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    fun onEvent(event : AddFishEvent){
        when (event) {
            is AddFishEvent.SetCity -> {
                if (state.value.fishFilterCity?.id == event.city?.id
                ) {
                    return
                }
                _state.value = state.value.copy(
                    fishFilterCity = event.city,
                )
                state.value.fish.area_kota = event.city.city
            }
            is AddFishEvent.SetSize -> {
                if (state.value.fishFilterSize == event.size
                ) {
                    return
                }
                _state.value = state.value.copy(
                    fishFilterSize = event.size,
                )
                state.value.fish.size = event.size.size
            }
            is AddFishEvent.InsertFish -> {
                insertFish(event.fish, event.context)
            }
            is AddFishEvent.ErrorInsertFish -> {
                viewModelScope.launch {
                    _eventFlow.emit(
                        UiEvent.ShowSnackbar(message = event.message)
                    )
                }
            }
        }
    }

    fun insertFish(fish : Fish,context : Context) {
        viewModelScope.launch {
            try {
                useCases.insertFish(fish)
                val work = OneTimeWorkRequestBuilder<CheckUnsentWorker>()
                    .setInputData(
                        Data.Builder()
                            .putString("data", Gson().toJson(fish))
                            .build())
                    .setConstraints(
                        Constraints.Builder()
                            .setRequiredNetworkType(NetworkType.CONNECTED)
                            .build()
                    ).build()
                WorkManager.getInstance(context).enqueue(work)
                _eventFlow.emit(UiEvent.SaveNote)
            } catch (e : Exception){
                e.printStackTrace()
                _eventFlow.emit(
                    UiEvent.ShowSnackbar(
                        message = e.message ?: "Couldn't save note"
                    )
                )
            }
        }

    }



    private var getCitiesJob: Job? = null
    private var getSizesJob: Job? = null
    fun getSizes(){
        getSizesJob?.cancel()
        getSizesJob = useCases.getSizes().onEach {value->
            _state.value = state.value.copy(
                sizes = value.filterNotNull(),
            )
        }.launchIn(viewModelScope)
    }

    fun getCities(){
        getCitiesJob?.cancel()
        getCitiesJob = useCases.getCities().onEach {value->
            _state.value = state.value.copy(
                cities = value.filterNotNull(),
            )
        }.launchIn(viewModelScope)
    }
    sealed class UiEvent {
        data class ShowSnackbar(val message: String): UiEvent()
        object SaveNote: UiEvent()
    }
}