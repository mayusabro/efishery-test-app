package com.test.efisheryapp.features.domain.entity.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.Gson
import java.text.NumberFormat
import java.util.*

@Entity(tableName = "sizes")
data class Size(
    @PrimaryKey
    val size : Int
) {
    fun getFormattedSize() = "%scm".format(NumberFormat.getNumberInstance(Locale.getDefault()).format(size))
}