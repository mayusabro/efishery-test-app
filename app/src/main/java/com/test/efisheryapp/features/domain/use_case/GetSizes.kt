package com.test.efisheryapp.features.domain.use_case

import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Size
import com.test.efisheryapp.features.domain.repository.FishRepository
import com.test.efisheryapp.features.domain.repository.SizeRepository
import kotlinx.coroutines.flow.map

class GetSizes(
    private val repository : SizeRepository
) {
    operator fun invoke()= repository.getSizes().map {
        mutableListOf<Size?>().apply {
            add(null)
            addAll(it.data?: emptyList())
        }
    }
}