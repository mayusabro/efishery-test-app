package com.test.efisheryapp.features.data.data_source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.efisheryapp.features.domain.entity.model.Fish

@Dao
interface FishDao {
    @Query("SELECT * FROM fishes")
    fun getFishes(): List<Fish>

    @Query("SELECT * FROM fishes WHERE synced = 0")
    fun getUnsentFishes(): List<Fish>

    @Query("SELECT * FROM fishes WHERE size = :size")
    fun getFishesBySize(size : Int): List<Fish>

    @Query("SELECT * FROM fishes WHERE area_kota = :city")
    fun getFishesByCity(city : String): List<Fish>

    @Query("SELECT * FROM fishes WHERE area_kota = :city AND size = :size")
    fun getFishesBySizeCity(size : Int, city : String): List<Fish>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllFish(list: List<Fish>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFish(fish : Fish)


}