package com.test.efisheryapp.features.presentation.fishes.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.test.efisheryapp.features.domain.entity.model.Fish
import java.text.NumberFormat
import java.util.*


@Composable
fun FishCard(
    modifier: Modifier = Modifier,
    fish : Fish,
) {
    Box(modifier = modifier
        .fillMaxWidth()
        .padding(horizontal = 16.dp, vertical = 0.dp)
        .shadow(2.dp, shape =  RoundedCornerShape(8.dp))
        .background(
            color = MaterialTheme.colors.background,
            shape = RoundedCornerShape(8.dp),
        )
    ) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 24.dp, vertical = 12.dp),
            verticalAlignment = Alignment.Bottom
        ) {
            Column(modifier = Modifier
                .weight(1f),
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = fish.komoditas?:"",
                    style = MaterialTheme.typography.body1,
                    maxLines = 1,
                    color = MaterialTheme.colors.onSurface
                )
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = "Rp%s/kg".format(NumberFormat.getNumberInstance(Locale.getDefault()).format(fish.price?:0)),
                    style = MaterialTheme.typography.h6,
                    maxLines = 1,
                    color = MaterialTheme.colors.onSurface
                )
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = "%scm".format(NumberFormat.getNumberInstance(Locale.getDefault()).format(fish.size?:0)),
                    style = MaterialTheme.typography.body2,
                    maxLines = 1,
                    color = MaterialTheme.colors.onSurface

                )
            }
            Column(modifier = Modifier
                .weight(1f)
                .padding(start = 4.dp)
            ) {

                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = fish.getParsedDate()?:"",
                    style = MaterialTheme.typography.overline,
                    textAlign = TextAlign.End,
                    maxLines = 1,
                    color = MaterialTheme.colors.onSurface

                )

                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = "%s, %s".format(fish.area_kota, fish.area_provinsi),
                    style = MaterialTheme.typography.caption,
                    textAlign = TextAlign.End,
                    maxLines = 1,
                    color = MaterialTheme.colors.onSurface

                )
            }

        }
    }
}