package com.test.efisheryapp.features.data.data_source.remote

import com.test.efisheryapp.BuildConfig
import com.test.efisheryapp.features.domain.PostResponse
import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Size
import com.test.efisheryapp.features.domain.entity.response.FishResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query


interface FishService {
    @GET("list")
    suspend fun getFishList(
        @Query("search") search: String = ""
    ): List<FishResponse>

    @GET("option_area")
    suspend fun getAreaList(): List<City>

    @GET("option_size")
    suspend fun getSizeList(): List<Size>

    @POST("list")
    suspend fun insertFish(@Body body: MutableList<FishResponse>): PostResponse
}