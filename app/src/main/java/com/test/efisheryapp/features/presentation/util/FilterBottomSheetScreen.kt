package com.test.efisheryapp.features.presentation.util

sealed class FilterBottomSheetScreen() {
    object CityScreen: FilterBottomSheetScreen()
    object SizeScreen: FilterBottomSheetScreen()
}