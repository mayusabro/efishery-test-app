package com.test.efisheryapp.core.network

enum class FailureStatus {

  API_FAIL,
  SERVER_SIDE_EXCEPTION,
  TOKEN_EXPIRED,
  NO_INTERNET,
  OTHER

}