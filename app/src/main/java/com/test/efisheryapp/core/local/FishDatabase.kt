package com.test.efisheryapp.core.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.test.efisheryapp.features.data.data_source.local.CityDao
import com.test.efisheryapp.features.data.data_source.local.FishDao
import com.test.efisheryapp.features.data.data_source.local.SizeDao
import com.test.efisheryapp.features.domain.entity.model.City
import com.test.efisheryapp.features.domain.entity.model.Fish
import com.test.efisheryapp.features.domain.entity.model.Size


@Database(entities = [Fish::class, City::class, Size::class], version = FishDatabase.DATABASE_VERSION)
abstract class FishDatabase : RoomDatabase() {

  companion object {
    const val DATABASE_VERSION = 5
    const val DATABASE_NAME = "FishDatabase"
  }

  abstract fun getFishDao(): FishDao
  abstract fun getSizeDao(): SizeDao
  abstract fun getCityDao(): CityDao
}