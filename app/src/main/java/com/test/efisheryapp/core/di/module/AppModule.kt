package com.test.efisheryapp.core.di.module

import android.content.Context
import androidx.room.Room
import com.test.efisheryapp.core.local.FishDatabase
import com.test.efisheryapp.features.data.data_source.remote.FishService
import com.test.efisheryapp.features.data.data_source.remote.FishesRemoteDataSource
import com.test.efisheryapp.features.data.repository.CityRepositoryImpl
import com.test.efisheryapp.features.data.repository.FishRepositoryImpl
import com.test.efisheryapp.features.data.repository.SizeRepositoryImpl
import com.test.efisheryapp.features.domain.repository.CityRepository
import com.test.efisheryapp.features.domain.repository.FishRepository
import com.test.efisheryapp.features.domain.repository.SizeRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

  @Provides
  @Singleton
  fun provideFishDatabase(@ApplicationContext context: Context): FishDatabase =
    Room.databaseBuilder(context, FishDatabase::class.java, FishDatabase.DATABASE_NAME)
      .fallbackToDestructiveMigration()
      .build()

  @Provides
  @Singleton
  fun provideFishRepository(rds: FishesRemoteDataSource, db : FishDatabase) : FishRepository{
    return FishRepositoryImpl(rds, db.getFishDao())
  }

  @Provides
  @Singleton
  fun provideFishesRemoteDataSource(service : FishService) : FishesRemoteDataSource{
    return FishesRemoteDataSource(service)
  }

  @Provides
  @Singleton
  fun provideCityRepository(rds: FishesRemoteDataSource, db : FishDatabase): CityRepository {
    return CityRepositoryImpl(rds, db.getCityDao())
  }



  @Provides
  @Singleton
  fun provideSizeRepository(rds: FishesRemoteDataSource, db : FishDatabase): SizeRepository {
    return SizeRepositoryImpl(rds, db.getSizeDao())
  }

}