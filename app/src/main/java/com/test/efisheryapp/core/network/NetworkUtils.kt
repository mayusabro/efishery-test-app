package com.test.efisheryapp.core.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.net.UnknownHostException

suspend fun <T> safeApiCall(apiCall: suspend () -> T): Resource<T> {
  return withContext(Dispatchers.IO) {
    try {
      val apiResponse: T = apiCall.invoke()

      if (apiResponse is List<*>) {
        if (apiResponse.isEmpty()) {
          Resource.empty()
        } else {
          Resource.success(apiResponse)
        }
      } else {
        Resource.failure(FailureStatus.API_FAIL, "Something wrong in server")
      }
    } catch (throwable: Throwable) {
      throwable.printStackTrace()
      when (throwable) {
        is HttpException -> {
          if (throwable.code() == 401) {
            Resource.failure(FailureStatus.TOKEN_EXPIRED,throwable.response()?.errorBody().toString())
          } else {
            Resource.failure(FailureStatus.SERVER_SIDE_EXCEPTION, throwable.response()?.errorBody().toString())
          }
        }

        is UnknownHostException -> {
          Resource.failure(FailureStatus.NO_INTERNET,  "No Internet")
        }

        else -> {
          Resource.failure(FailureStatus.OTHER, "Something wrong")
        }
      }
    }
  }
}

fun <T, A> performGetOperation(databaseQuery: () -> T,
                               networkCall: suspend () -> Resource<A>,
                               saveCallResult: suspend A.() -> Unit)  =
  flow{
    emit(Resource.loading())
    emit(Resource.success(databaseQuery()))
    val responseStatus = networkCall()

    //Cache to database if response is successful
    if (responseStatus.status == Resource.Status.SUCCESS) {
      responseStatus.data?.apply {
        saveCallResult(this)
      }

    } else if (responseStatus.status == Resource.Status.ERROR) {
      emit(
        Resource.failure(
          responseStatus.failureStatus ?: FailureStatus.OTHER,
          responseStatus.message ?: ""
        )
      )
    }
    emit(Resource.success(databaseQuery()))
  }.flowOn(Dispatchers.IO)

fun <T> performPostOperation(saveCallResult: suspend () -> Unit,
                               networkCall: suspend () -> Unit
) =
  flow {
    emit(Resource.loading())
    emit(Resource.success(saveCallResult()))
    networkCall()
  }.flowOn(Dispatchers.IO)


