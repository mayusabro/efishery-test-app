package com.test.efisheryapp.core.di.module

import com.test.efisheryapp.features.domain.repository.CityRepository
import com.test.efisheryapp.features.domain.repository.FishRepository
import com.test.efisheryapp.features.domain.repository.SizeRepository
import com.test.efisheryapp.features.domain.use_case.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object UseCaseModule {
    @Provides
    @Singleton
    fun provideFishUseCases(fishRepository: FishRepository, cityRepository: CityRepository, sizeRepository: SizeRepository) = FishUseCases(GetFishes(fishRepository), GetSizes(sizeRepository), GetCities(cityRepository))

    @Provides
    @Singleton
    fun provideInsertFishUseCases(fishRepository: FishRepository, cityRepository: CityRepository, sizeRepository: SizeRepository) = InsertFishUseCases(InsertFishToLocal(fishRepository), GetSizes(sizeRepository), GetCities(cityRepository))

}