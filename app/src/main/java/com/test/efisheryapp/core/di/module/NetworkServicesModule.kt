package com.test.efisheryapp.core.di.module

import com.test.efisheryapp.features.data.data_source.remote.FishService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = [RetrofitModule::class])
@InstallIn(SingletonComponent::class)
object NetworkServicesModule {

  @Provides
  @Singleton
  fun provideFishServices(retrofit: Retrofit): FishService {
    return retrofit.create(FishService::class.java)
  }
}