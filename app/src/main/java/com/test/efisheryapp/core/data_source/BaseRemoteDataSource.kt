package com.test.efisheryapp.core.data_source

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import com.test.efisheryapp.core.network.FailureStatus.API_FAIL
import com.test.efisheryapp.core.network.FailureStatus.NO_INTERNET
import com.test.efisheryapp.core.network.FailureStatus.OTHER
import com.test.efisheryapp.core.network.FailureStatus.SERVER_SIDE_EXCEPTION
import com.test.efisheryapp.core.network.FailureStatus.TOKEN_EXPIRED
import com.test.efisheryapp.core.network.Resource
import com.test.efisheryapp.core.network.ResponseStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.net.UnknownHostException
import javax.inject.Inject

open class BaseRemoteDataSource @Inject constructor() {





}
