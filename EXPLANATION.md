# Penjelasan Desain

Menampilkan harga, ukuran, wilayah dari setiap ikan

## Warna
Warna hijau cerah yang identik dengan eFishery menjadi warna utama aplikasi ini

## Tata letak
Tata letak aplikasi ini diterapkan simpel sesuai dengan fungsi dari aplikasi yang cukup simpel.

## Bentuk / Shape
Bentuk 'Rounded' pada setiap container adalah tema dari aplikasi ini, yang menimbulkan tampilan smooth, simpel, dan elegan.

## Font
Font yang dipakai adalah font Montserrat yang merupakan font dengan bentuk simpel sesuai dengan tema aplikasi ini.